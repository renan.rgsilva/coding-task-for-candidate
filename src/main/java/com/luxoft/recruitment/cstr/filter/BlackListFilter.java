package com.luxoft.recruitment.cstr.filter;

import java.util.HashSet;
import java.util.Set;

import com.luxoft.recruitment.cstr.http.Request;
import com.luxoft.recruitment.filter.dao.BlackListFilterDAO;

public class BlackListFilter {

	private final Set<String> blackList;
	private BlackListFilterDAO blackListFilterDAO;

	public BlackListFilter() {
		blackList = new HashSet<>();
		blackList.add("74.125.224.72");
	}

	public BlackListFilter(Set<String> preloadedBlackList) {
		this.blackList = preloadedBlackList;
		loadPredefinedIP(blackList);
	}

	public boolean shouldBlock(Request request) {
		if (blackList.contains(request.getIpAddress()) || blackListFilterDAO.isBlocked(request)) {
			return true;
		}
		return false;
	}

	public void block(Request request) {
		validateRequest(request);
		blackListFilterDAO.block(request);
	}

	public void unblock(Request request) {
		validateRequest(request);
		blackList.remove(request.getIpAddress());
		blackListFilterDAO.unblock(request);
	}

	public void setBlackListFilterDAO(BlackListFilterDAO blackListFilterDAO) {
		this.blackListFilterDAO = blackListFilterDAO;
	}

	private void validateRequest(Request request) {
		if (request == null || request.getIpAddress() == null || "".equals(request.getIpAddress()))
			throw new IllegalArgumentException("IP must be informed.");
	}

	public void loadPredefinedIP(Set<String> setIp) {
		this.blackListFilterDAO.loadPredefinedIP(setIp);
	}

}
