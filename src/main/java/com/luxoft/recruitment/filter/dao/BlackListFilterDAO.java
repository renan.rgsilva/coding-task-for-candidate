package com.luxoft.recruitment.filter.dao;

import java.util.Set;

import com.luxoft.recruitment.cstr.http.Request;

public interface BlackListFilterDAO {

	public void block(Request request);

	public void unblock(Request request);

	public boolean isBlocked(Request request);

	public void loadPredefinedIP(Set<String> setIp);

}
