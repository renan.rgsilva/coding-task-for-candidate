package com.luxoft.recruitment.filter;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.luxoft.recruitment.cstr.filter.BlackListFilter;
import com.luxoft.recruitment.cstr.http.Request;

@RunWith(JUnit4.class)
public class BlackListFilterTest {

	private BlackListFilter blackListFilter;

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Before
	public void setup() {
		blackListFilter = new BlackListFilter(); // mock(BlackListFilter.class);
	}

	
	
	public void testBlockSuccessful() {
		Request request = new Request("111.111.111.111");
		blackListFilter.block(request);
	}

	@Test
	public void testBlockExceptionRequestNull() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilter.block(null);
	}

	@Test
	public void testUnblockExceptionRequestNull() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilter.unblock(null);
	}

	@Test
	public void testBlockExceptionNullIP() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilter.block(new Request(null));
	}

	@Test
	public void testUnblockExceptionNullIP() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilter.unblock(new Request(null));
	}

	@Test
	public void testBlockExceptionEmptyIP() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilter.block(new Request(""));
	}

	@Test
	public void testUnblockExceptionEmptyIP() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilter.unblock(new Request(""));
	}

}
