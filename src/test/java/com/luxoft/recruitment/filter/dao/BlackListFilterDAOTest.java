package com.luxoft.recruitment.filter.dao;

import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.luxoft.recruitment.cstr.http.Request;

public class BlackListFilterDAOTest {

	@Rule
	ExpectedException exception = ExpectedException.none();

	private BlackListFilterDAO blackListFilterDAO;

	@Before
	public void setup() {
		blackListFilterDAO = mock(BlackListFilterDAO.class);
	}

	@Test
	public void testBlockExceptionRequestNull() {
		doThrow(new IllegalArgumentException("IP must be informed.")).when(blackListFilterDAO).block(isNull());
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilterDAO.block(null);
	}

	@Test
	public void testBlockExceptionEmptyIP() {
		doThrow(new IllegalArgumentException("IP must be informed.")).when(blackListFilterDAO).block(new Request(""));
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilterDAO.block(new Request(""));
	}

	@Test
	public void testUnblockExceptionRequestNull() {
		doThrow(new IllegalArgumentException("IP must be informed.")).when(blackListFilterDAO).unblock(isNull());
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilterDAO.unblock(null);
	}

	@Test
	public void testUnblockExceptionEmptyIP() {
		doThrow(new IllegalArgumentException("IP must be informed.")).when(blackListFilterDAO).unblock(new Request(""));
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilterDAO.unblock(new Request(""));
	}

	@Test
	public void testBlockExceptionNullIP() {
		doThrow(new IllegalArgumentException("IP must be informed.")).when(blackListFilterDAO).block(new Request(null));
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilterDAO.block(new Request(null));
	}

	@Test
	public void testUnblockExceptionNullIP() {
		doThrow(new IllegalArgumentException("IP must be informed.")).when(blackListFilterDAO)
				.unblock(new Request(null));
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("IP must be informed.");
		blackListFilterDAO.unblock(new Request(null));
	}

	@Test
	public void loadPredefinedIP() {
		doThrow(new IllegalArgumentException()).when(blackListFilterDAO).loadPredefinedIP(isNull());
	}
}
